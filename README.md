### Notes

### Installing ansible

### Configuring AWS


### Scripts

  1. Create a base AMI

    `cloud/create-base-ami.yml`

     + Takes the latest ubuntu Image in the Amazon Public AMIs
     + Installs Ubuntu Packages.
     + Installs PIP Dependencies.



    List of Base AMIs can be seen via

    `cloud/list-base-ami.yml`

  2. Create a web AMI/deployable AMI

     `cloud/create-ami.yml`

      Takes the base AMI Created in the Step #1.

      + Installs the latest code.
      + Setup mod wsgi express
      + Setup newrelic and cloud watch

      Example

      `ansible-playbook --extra-vars '{"ami": "ami-name"}' create-ami.yml`  

       List of Web AMIs can be seen via

      `cloud/list-web-ami.yml`

  3. Set up the VPC and Subnets for the environment        
      Creates a VPC, Public and Private Subnets.  

      `cloud/create-vpc.yml`

      Example

      `ansible-playbook --extra-vars '{"ENV":"test"}' create-vpc.yml`


 4.  Start the blue/violet configuration    

  	+ Start the NAT Server in private subnet if not running.
 	+ Creates Auto Configuration Group. The details and parameters of the auto-configuration
 	group are defined in the "environment_{{ENV}}" files.
 	+ Creates the Launch Configuration.
 	+ Creates the Load Balancers.
 	+ Sets up the routing between the private and public subnet.
 	+ Destroys the Relative Configuration.


	Parameters

	| Parameters  | Required/Optional | Values  | Notes |
	|---|---|---|---|
	| ENV  | Optional  | Default Value: test   | Environment to run the configuration in.   |   
	|  image_id | Required  | Value of the ami to use to start this configuration.  | To see latest amis use the `list_web_ami.yml`   |   
	|  terminate_relative | Required  | Default Value: yes  | Used to destroy the violet auto configuration group if active. Typically scenarios of having two groups is not yet tested.  |    



	Examples

	Run the Blue Configuration

	`ansible-playbook -vvvv --extra-vars '{"image_id":"ami-4cd4db26", "terminate_relative": "yes", "ENV": "test"}' start-blue.yml`


	Run the Violet Configuration

	`ansible-playbook -vvvv --extra-vars '{"image_id":"ami-4cd4db26", "terminate_relative": "yes", "ENV": "test"}' start-violet.yml`


	If an alternate configuration is running the above scripts allow for a rolling update. Otherwise they just setup the initial environment. Make sure the VPCs are setup for the environment.

 5. Destroy the configuration.
    + destroys the auto-scale group
    + brings down all the instances.
    + elb, auto scale groups, security groups, nat instances, are all destroyed.
    + color is defaulted to blue


  Examples:

  `ansible-playbook destroy.yml`: - kills the blue configuration

  `ansible-playbook --extra-var='{"color": "violet"}' destroy.yml` : - kills the violet configuration


 | Parameter  |  Required/Optional | Values  |  Notes |   
|---|---|---|---|---|
| ENV  |  Optional | Default: test  |  Environment to run the configuration in|
|   color | Optional  | Default: blue   |  Which ASG group to delete.  |

### SSH into the servers
+ Use ssh-add to add the ssh identity

  `ssh-add <key_name>`

+ SSH into the NAT Server

  `ssh -A ec2-user@<nat_server_dns or nat_server_ip>`

  -A option forwards the host keys to the NAT Server

+ SSH into individual machines

  `ssh ubuntu@<web_server_public_ip or web_server_private_ip>`
